var Kids = require('../models/userModel');


module.exports = function(app){
    app.get('/api/defaultKids', function(req, res){
        //seed database
       var defaultKids = [
            {
                username: "littleann",
                password: "123456",
                firstName: "Anna",
                lastName: "Romancheko",
                image: "images/anna.jpg",
                points: 15,
                isKid: true
            },
            {
                username: "littledan",
                password: "123456",
                firstName: "Daniel",
                lastName: "Romancheko",
                image: "images/daniel.jpg",
                points: 20,
                isKid: true
            },
            {
                username: "littleyana",
                password: "123456",
                firstName: "Yana",
                lastName: "Romancheko",
                image: "images/yana.jpg",
                points: 20,
                isKid: true
            }
        ]
        
        Kids.create(defaultKids, function(err, results){
            if(err) throw err;
            res.send(results);
        });

    });
    
    app.get('/api/clearKids', function(req, res){
        //seed database
       
        Kids.remove({}, function(err){
            if(err) throw err;
            res.send("DB is cleared!");
        });

    });
}