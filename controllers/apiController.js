var Kids = require('../models');
var bodyParser = require('body-parser');
var log = require('../libs/log')(module);

module.exports = function(app){

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: true }));
    
    //KIDS INDEX ROUTE
    app.get('/api/kids', function(req, res) 
    {
        Kids.showAllUsers(function(err, users){
             if(!err){
                res.json(users);
            } else{
                res.statusCode = 500;
                //log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
        
    })
    
    //KIDS SHOW ROUTE
    app.get('/api/kids/:id', function(req, res){

        return Kids.showUserById(req.params.id, function(err, kid){
            if(!kid){
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            }
            if(!err){
                res.json(kid);
            } else {
                res.statusCode = 500;
                //log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
            }
        });
    });
    
    //KID CREATE ROUTE
    app.post('/api/kids', function(req, res){
            var user = {
                username: req.body.username,
                password: req.body.password,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                image: req.body.image,
                points: req.body.points,
                isKid: req.body.isKid
            }
            Kids.createNewUser(user, function(err, user){
                      if(!err)
                        {
                        log.info("Kid Successfuly created");
                        res.json({message: 'Kid Successfuly created', status: 'OK', user });  
                        } else{
                            console.log(err);
                    if(err.name == 'ValidationError') {
                        res.statusCode = 400;
                        res.send({ error: 'Validation error' });
                    } else {
                        res.statusCode = 500;
                        res.send({ error: 'Server error' });
                    }
                log.error('Internal error(%d): %s', res.statusCode, err);
                }
            })
    });
    
    app.put('/api/kids/:kid', function(req, res){
        var user = {
                username: req.body.username,
                password: req.body.password,
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                image: req.body.image,
                points: req.body.points,
                isKid: req.body.isKid
            }
        
        Kids.updateUser(req.params.kid, user, function(err, updatedUser){
                if(!updatedUser){
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            } else {
                if(!err){
                    log.info("User updated");
                    res.json ({message: 'Successfuly Updated User', updatedUser});
                  
                } else {
                    if(err.name == 'ValidationError') {
                        res.statusCode = 400;
                        res.send({ error: 'Validation error' });
                    } else {
                        res.statusCode = 500;
                        res.send({ error: 'Server error' });
                    }
                log.error('Internal error(%d): %s',res.statusCode,err.message);
                }
            }
        })
    });

    app.delete('/api/kids/:kid', function(req, res){
        return Kids.deleteUser(req.params.kid, function(err, user){
            if(!user){
                res.statusCode = 404;
                return res.send({ error: 'Not found' });
            } else {
            if(!err){  
            //    log.info("article removed");
                return res.send({message: 'Successfuly Deleted', status: 'OK', user});
            } else {
                res.statusCode = 500;
          //      log.error('Internal error(%d): %s',res.statusCode,err.message);
                return res.send({ error: 'Server error' });
                }
            }
        });
    });
}