var express = require("express");
var app = express();

var bodyParser = require("body-parser");


var mongoose = require("mongoose");
var config = require('./libs/config');

var oauth2 = require('./libs/auth/oauth2');
var passport = require("passport");
require('./libs/auth/auth');


var methodOverride = require("method-override");

var setupController = require('./controllers/setupController');
var apiController = require('./controllers/apiController');

//Adding Winston Logger from file
var log = require('./libs/log')(module);
var morgan = require('morgan');

app.use(morgan('dev'));

//DB Settings
// var url = process.env.DATABASEURL || "mongodb://localhost/moms_helper";
// mongoose.connect(url);
mongoose.connect(config.get('mongoose:uri'));

//app config
app.use(bodyParser.urlencoded({extended: true}));
app.set("view engine", "ejs");
app.use(methodOverride("_method"));

//Custom Stylesheets settings
app.use(express.static(__dirname+"/public"));

var kids = [
    {name: "Anna", image: "images/anna.jpg", points: 20},
    {name: "Daniel", image: "images/daniel.jpg", points: 20},
    {name: "Yana", image: "images/yana.jpg", points: 20}
    ]

setupController(app);
apiController(app);

//Root route
app.get("/", function(req, res){
    res.render("landing");
})

//get token route
app.post('/oauth/token', oauth2.token);

//get userInfo
app.get('/api/userInfo',
    passport.authenticate('bearer', { session: false }),
        function(req, res) {
            // req.authInfo is set using the `info` argument supplied by
            // `BearerStrategy`.  It is typically used to indicate scope of the token,
            // and used in access control checks.  For illustrative purposes, this
            // example simply returns the scope in the response.
            res.json({ user_id: req.user.userId, name: req.user.username, scope: req.authInfo.scope })
        }
);


//Index route
app.get("/kids", function(req, res){
    res.render("kids", {kids: kids});
})

app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});

app.listen(process.env.PORT, process.env.IP, function(){
    log.info("App is running!");
});



module.exports = app; // for testing