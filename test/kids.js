//During the test the env variable is set to test
process.env.NODE_ENV = 'test';


var mongoose = require("mongoose");
var Kids = require('../models/userModel');
var ClientModel = require('../models/clientModel');
var AccessTokenModel        = require('../models/accessTokenModel');
var RefreshTokenModel       = require('../models/refreshTokenModel');


var chai = require('chai');
var chaiHttp = require('chai-http');
var app = require('../app');
var should = chai.should();

chai.use(chaiHttp);

describe('Kids', function() {
    beforeEach(function(done)
    {
        Kids.remove({}, function(err){ 
            if(err) throw err;
            done();         
        });     
    });
    
  describe('GET /api/kids', function(){
      it('it should GET all the Kids', function(done){
        chai.request(app)
            .get('/api/kids')
            .end(function(err, res){
                if(err) throw err;
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
              done();
            });
      });
  });
  
  describe('POST /api/kids', function(){
      it('it should CREATE Kid', function(done){
          var newKid = {
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          }
          
        chai.request(app)
            .post('/api/kids')
            .send(newKid)
            .end(function(err, res){
                if(err) throw err;
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql('Kid Successfuly created');
                res.body.user.should.have.property('lastName');
                res.body.user.should.have.property('image');
                res.body.user.should.have.property('points');
              done();
            });
      });
  });
  
  describe('POST /api/kids without name', function(){
      it('it should return ERROR', function(done){
        
        var newKid = {
              "username": "littleann",
              "password": "123456",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          }
          
        chai.request(app)
            .post('/api/kids')
            .send(newKid)
            .end(function(err, res){
                err.should.have.status(400);
                res.body.should.have.property('error').eql("Validation error");
                done();
            });
      });
  });
  
  describe('POST /api/kids with empty string instead of image', function(){
      it('it should return ERROR', function(done){
          var newKid = {
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "",
              "points": 15,
              "isKid": true
          }
          
        chai.request(app)
            .post('/api/kids')
            .send(newKid)
            .end(function(err, res){
                err.should.have.status(400);
              done();
            });
      });
  });
  
  describe('POST /api/kids with empty string instead of points', function(){
      it('it should return ERROR', function(done){
          var newKid = {
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": "",
              "isKid": true
          }
          
        chai.request(app)
            .post('/api/kids')
            .send(newKid)
            .end(function(err, res){
                err.should.have.status(400);
                done();
            });
      });
  });
  
  
  describe('GET /api/kids/:kid ', function(){
      it('it should return kid details', function(done){
          var newKid = new Kids({
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          });
            newKid.save(function(err, kid){
                chai.request(app)
                .get('/api/kids/' + kid.id)
                .end(function(err, res){
                    if(err) throw err;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('username');
                    res.body.should.have.property('image');
                    res.body.should.have.property('points');
                    res.body.should.have.property('_id').eql(kid.id);
                    done();
                });
            })
      });
  });
  
  
  describe('/PUT /api/kids/:kid ', function(){
      it('it should return updated kid', function(done){
          var newKid = new Kids({
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          });
          var kidToUpdate = {
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 25,
              "isKid": true
          };
            newKid.save(function(err, kid){
                chai.request(app)
                .put('/api/kids/' + kid.id)
                .send(kidToUpdate)
                .end(function(err, res){
                    if(err) throw err;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('updatedUser');
                    res.body.should.have.property('message').eql('Successfuly Updated User');
                    res.body.updatedUser.should.have.property('username');
                    res.body.updatedUser.should.have.property('image');
                    res.body.updatedUser.should.have.property('points').eql(kidToUpdate.points);
                    res.body.updatedUser.should.have.property('_id').eql(kid.id);
                    done();
                });
            })
      });
  });
  
  describe('DELETE /api/kids/:kid', function(){
      it('it should Delete kid', function(done){
          var newKid = new Kids({
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          });
            newKid.save(function(err, kid){
                chai.request(app)
                .delete('/api/kids/'+ kid.id)
                .end(function(err, res){
                    if(err) throw err;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql('Successfuly Deleted');
                    res.body.should.have.property('user');
                    res.body.user.should.have.property('username').eql(kid.username);
                    res.body.user.should.have.property('_id').eql(kid.id);
                    done();
                });
            })
      });
  });
  
  
  describe('POST /oauth/token', function(){
      it('it should return Access Token and Refresh Token for user', function(done){
       
        ClientModel.remove({}, function(err) {
            var client = new ClientModel(
                {
                    name: "OurService iOS client v1",
                    clientId: "mobileV1",
                    clientSecret:"abc123456"
                });
            client.save(function(err, client) {
                if(err) return err;
                else console.log("New client - %s:%s",client.clientId,client.clientSecret);
            });
        });
        
        AccessTokenModel.remove({}, function (err) {
                if (err) return err;
                });
        RefreshTokenModel.remove({}, function (err) {
            if (err) return err;
            });
            
          var newKid = new Kids({
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          });
          var tokenRequest = {
              grant_type:"password",
              client_id:"mobileV1",
              client_secret:"abc123456",
              username: "littleann",
              password: "123456"
          };
            newKid.save(function(err, kid){
                chai.request(app)
                .post('/oauth/token')
                .send(tokenRequest)
                .end(function(err, res){
                    if(err) throw err;
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('access_token');
                    res.body.should.have.property('refresh_token');
                    res.body.should.have.property('expires_in').eql(3600);
                    res.body.should.have.property('token_type').eql("Bearer");
                    done();
                });
            })
      });
  });
  
  describe('POST /oauth/token with RefreshToken', function(){
      it('it should return Access Token and Refresh Token for user', function(done){
       
        ClientModel.remove({}, function(err) {
            var client = new ClientModel(
                {
                    name: "OurService iOS client v1",
                    clientId: "mobileV1",
                    clientSecret:"abc123456"
                });
            client.save(function(err, client) {
                if(err) return err;
                else console.log("New client - %s:%s",client.clientId,client.clientSecret);
            });
        });
        
        AccessTokenModel.remove({}, function (err) {
                if (err) return err;
                });
        RefreshTokenModel.remove({}, function (err) {
            if (err) return err;
            });
            
          var newKid = new Kids({
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          });
          var tokenRequest = {
              grant_type:"password",
              client_id:"mobileV1",
              client_secret:"abc123456",
              username: "littleann",
              password: "123456"
          };
            newKid.save(function(err, kid){
                chai.request(app)
                .post('/oauth/token')
                .send(tokenRequest)
                .end(function(err, res){
                    var refreshRequest = {
                      grant_type:"refresh_token",
                      client_id:"mobileV1",
                      client_secret:"abc123456",
                      refresh_token:res.body.refresh_token
                    }
                    chai.request(app)
                    .post('/oauth/token')
                    .send(refreshRequest)
                    .end(function(err, res){
                        if(err) throw err;
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('access_token');
                        res.body.should.have.property('refresh_token');
                        res.body.should.have.property('expires_in').eql(3600);
                        res.body.should.have.property('token_type').eql("Bearer");
                        done();
                });
            })
      });
  });
  });
  
  //End bracket
});