var mongoose = require('mongoose');
var log = require('../libs/log')(module);
var crypto = require('crypto');

var Schema = mongoose.Schema;


// AccessToken
var AccessToken = new Schema({
    userId: {
        type: String,
        required: true
    },
    clientId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

var AccessTokenModel = mongoose.model('AccessToken', AccessToken);

module.exports = AccessTokenModel;