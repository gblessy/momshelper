var UserModel = require("./userModel");

var showAllUsers = function(cb)
{
    UserModel.find({}, cb)
};

var showUserById = function(user_id, cb){
    UserModel.findById({_id: user_id}, cb);
};

var createNewUser = function(user, cb){
    var newUser = UserModel({
                username: user.username,
                password: user.password,
                firstName: user.firstName,
                lastName: user.lastName,
                image: user.image,
                points: user.points,
                isKid: user.isKid
    })
    newUser.save(cb);
};

var updateUser = function(id, user, cb){
    UserModel.findById(id, function(err, oldUser){
        if(err){
            return (err, null);
        }
                oldUser.username= user.username;
                oldUser.password= user.password;
                oldUser.firstName= user.firstName;
                oldUser.lastName= user.lastName;
                oldUser.image= user.image;
                oldUser.points= user.points;
                oldUser.isKid= user.isKid;
                
            oldUser.save(cb);
        });
    };

var deleteUser = function (user_id, cb){
    UserModel.findById(user_id, function(err, user){
        if(err){
            return (err, null);
        }
            return user.remove(cb);
        });
}

module.exports = UserModel;
module.exports.showAllUsers = showAllUsers;
module.exports.showUserById = showUserById;
module.exports.createNewUser = createNewUser;
module.exports.updateUser = updateUser;
module.exports.deleteUser = deleteUser;
