var mongoose = require('mongoose');
var log = require('../libs/log')(module);
var crypto = require('crypto');

var Schema = mongoose.Schema;

// RefreshToken
var RefreshToken = new Schema({
    userId: {
        type: String,
        required: true
    },
    clientId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

var RefreshTokenModel = mongoose.model('RefreshToken', RefreshToken);
module.exports = RefreshTokenModel;