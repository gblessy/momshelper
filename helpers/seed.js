var log                 = require('../libs/log')(module);
var mongoose            = require('mongoose');
var UserModel           = require('../models');
var ClientModel         = require('../models/clientModel');
var AccessTokenModel    = require('../models/accessTokenModel');
var RefreshTokenModel   = require('../models/refreshTokenModel');
//var faker               = require('Faker');
var config = require('../libs/config');

mongoose.connect(config.get('mongoose:uri'));


UserModel.remove({}, function(err) {
    
    var user = new UserModel({
              "username": "littleann",
              "password": "123456",
              "firstName": "Anna",
              "lastName": "Romancheko",
              "image": "images/anna.jpg",
              "points": 15,
              "isKid": true
          });
    
    user.save(function(err, user) {
        if(err) return log.error(err);
        else log.info("New user - %s:%s",user.username,user.password);
    });

    // for(i=0; i<4; i++) {
    //     var user = new UserModel({ username: faker.random.first_name().toLowerCase(), password: faker.Lorem.words(1)[0] });
    //     user.save(function(err, user) {
    //         if(err) return log.error(err);
    //         else log.info("New user - %s:%s",user.username,user.password);
    //     });
    // }
});

ClientModel.remove({}, function(err) {
    var client = new ClientModel({ name: "OurService iOS client v1", clientId: "mobileV1", clientSecret:"abc123456" });
    client.save(function(err, client) {
        if(err) return log.error(err);
        else log.info("New client - %s:%s",client.clientId,client.clientSecret);
    });
});
AccessTokenModel.remove({}, function (err) {
    if (err) return log.error(err);
});
RefreshTokenModel.remove({}, function (err) {
    if (err) return log.error(err);
});

setTimeout(function() {
    mongoose.disconnect();
}, 3000);